package com.example.shoppingcartyat;

import java.io.Serializable;

public class ShoppingItem implements Serializable {
    public String itemName;
    public int itemPrice;

    public ShoppingItem(String itemName, int itemPrice) {
        this.itemName = itemName;
        this.itemPrice = itemPrice;
    }
}
