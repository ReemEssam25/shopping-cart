package com.example.shoppingcartyat;

import java.io.Serializable;
import java.util.List;

public class ShoppingItemWrapper implements Serializable {
    public List<ShoppingItem> shoppingItemList;

    public ShoppingItemWrapper(List<ShoppingItem> shoppingItemList) {
        this.shoppingItemList = shoppingItemList;
    }
}
