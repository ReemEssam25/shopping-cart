package com.example.shoppingcartyat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.zip.Inflater;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    List<ShoppingItem> shoppingItems;

    public RecyclerAdapter(List<ShoppingItem> shoppingItems) {
        this.shoppingItems = shoppingItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.viewholder_shopping_item , parent,false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    ShoppingItem item;
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        item = shoppingItems.get(position);
        holder.tvItemName.setText(item.itemName);
        holder.tvItemPrice.setText(item.itemPrice + "L.E");
        holder.imageButton_Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shoppingItems.remove(item);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return shoppingItems.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        TextView tvItemName;
        TextView tvItemPrice;
        ImageButton imageButton_Delete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.tvItemName = itemView.findViewById(R.id.tvItemName);
            this.tvItemPrice = itemView.findViewById(R.id.tvItemPrice);
            this.imageButton_Delete = itemView.findViewById(R.id.imageButton_delete);
        }
    }
}
