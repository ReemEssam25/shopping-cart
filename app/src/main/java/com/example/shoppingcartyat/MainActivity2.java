package com.example.shoppingcartyat;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class MainActivity2 extends AppCompatActivity {

    TextView recite_tv;
    Button back_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        recite_tv = findViewById(R.id.recite_tv);
        back_button = findViewById(R.id.back_button);

        ShoppingItemWrapper shoppingItemWrapper = (ShoppingItemWrapper) getIntent()
                .getSerializableExtra(MainActivity.DATA_KEY);

        Map <ShoppingItem , Integer > itemMap = new HashMap<>();

        for (int i = 0 ; i < shoppingItemWrapper.shoppingItemList.size() ; i++)
        {
            Integer count = itemMap.get(shoppingItemWrapper.shoppingItemList.get(i));
            if (count == null)
                count = 1;
            else
                count ++;

            itemMap.put(shoppingItemWrapper.shoppingItemList.get(i), count);
        }

        String recite = "";
        int totalPrice = 0;

        for (Map.Entry<ShoppingItem, Integer> entry :itemMap.entrySet())
        {
            ShoppingItem item = entry.getKey();
            int quantity = entry.getValue();
            recite += item.itemName + " :        QTY: " + quantity +"       Price: " + (quantity*item.itemPrice) +"\n";
            totalPrice += (quantity*item.itemPrice);
        }

        recite+="\nTotal Price : " + totalPrice;

        recite_tv.setText(recite);

        back_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity2.this, "Paymant Done" , Toast.LENGTH_LONG).show();
                Intent intent = new Intent();
                setResult(RESULT_OK);
                finish();
            }
        });

    }
}