package com.example.shoppingcartyat;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Spinner spinner ;
    ImageButton imageButton;
    FloatingActionButton floatingActionButton;
    RecyclerView rv;
    ArrayList<ShoppingItem> spinnerItems;
    ArrayList<ShoppingItem> shoppingItems;
    RecyclerAdapter recyclerAdapter;
    public static final String DATA_KEY = "data";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = findViewById(R.id.spinner);
        imageButton = findViewById(R.id.imageButton);
        floatingActionButton = findViewById(R.id.floatingActionButton);
        rv= findViewById(R.id.rv);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShoppingItem item = (ShoppingItem) spinner.getSelectedItem();
                shoppingItems.add(item);
                recyclerAdapter.notifyDataSetChanged();
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this , MainActivity2.class);
                ShoppingItemWrapper shoppingItemWrapper = new ShoppingItemWrapper(shoppingItems);
                intent.putExtra(DATA_KEY,shoppingItemWrapper);
                startActivityForResult(intent, 1);
            }
        });

        setupSpinner();
        setupRecycler();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1)
        {
            shoppingItems.clear();
            recyclerAdapter.notifyDataSetChanged();
        }
    }

    private void setupRecycler() {
        shoppingItems = new ArrayList<>();

        //ShoppingItem item1 = new ShoppingItem("Apple" , 10);
        //shoppingItems.add(item1);
        recyclerAdapter = new RecyclerAdapter (shoppingItems);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.setAdapter(recyclerAdapter);
    }

    private void setupSpinner() {
        spinnerItems = new ArrayList<>();

        ShoppingItem item1 = new ShoppingItem("Apple" , 10);
        ShoppingItem item2 = new ShoppingItem("Meat" , 100);
        ShoppingItem item3 = new ShoppingItem("Fish" , 50);
        ShoppingItem item4 = new ShoppingItem("Rice" , 15);

        spinnerItems.add(item1);
        spinnerItems.add(item2);
        spinnerItems.add(item3);
        spinnerItems.add(item4);

        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(this, R.layout.spinner_item,spinnerItems);
        spinner.setAdapter(spinnerAdapter);

        int count = spinnerAdapter.getCount();


        /*spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(), spinnerItems.get(position).itemName, Toast.LENGTH_LONG).show();
            }
        });*/
    }
}