package com.example.shoppingcartyat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class SpinnerAdapter extends ArrayAdapter {
    List<ShoppingItem> objects;

    public SpinnerAdapter(@NonNull Context context, int resource, @NonNull List<ShoppingItem> objects) {
        super(context, resource, objects);
        this.objects = objects;
    }

    public View getCustomView(int position,ViewGroup parent) //ViewGroup = Layout
    {
        //attach the view to the adapter
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View layout = inflater.inflate(R.layout.spinner_item, parent, false);

        ShoppingItem shoppingItem = objects.get(position);

        //get the views by ids
        TextView tv_item_Name = layout.findViewById(R.id.tv_item_Name);
        TextView tv_item_price = layout.findViewById(R.id.tv_item_price);

        //attach the values to the views
        tv_item_Name.setText(shoppingItem.itemName);
        tv_item_price.setText(shoppingItem.itemPrice+" L.E");
        return layout;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        return getCustomView(position, parent);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        return getCustomView(position, parent);
    }

    @Override
    public int getCount() {
        return objects.size();
    }
}
